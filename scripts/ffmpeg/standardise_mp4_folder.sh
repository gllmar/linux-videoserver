#!/bin/bash
## $1 = folder to convert recursivly

STANDARDIZE()
{
FILENAME=`basename "$1"`
FILENAME=${FILENAME%.*}
DIRECTORY=`dirname "$1"`
echo "$DIRECTORY" "$FILENAME" 
ffmpeg -i "$1" -pix_fmt yuv420p "$DIRECTORY"/"std_${FILENAME}.mp4"
}

IFS=$'\n'; set -f
for f in $(find "$1" -name '*.mp4'); do STANDARDIZE "$f"; done
unset IFS; set +f


