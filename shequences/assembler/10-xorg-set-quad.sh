#!/bin/bash
echo "$DISPLAY"

if ! xset q &>/dev/null; then
    echo "No X server at \$DISPLAY [$DISPLAY]" >&2
	echo "sleep"
    sleep 20
    echo "woke"
    DISPLAY=:0
fi

echo "$DISPLAY"

xrandr \
--output DP-0 --off \
--output DP-1 --mode 1920x1080 --pos 1920x0 --rotate normal \
--output HDMI-0 --mode 1920x1080 --pos 0x1080 --rotate normal \
--output DP-2 --off \
--output DP-3 --mode 1920x1080 --pos 0x0 --rotate normal \
--output DP-4 --off \
--output DP-5 --mode 1920x1080 --pos 1920x1080 --rotate normal \
--output USB-C-0 --off
sleep 1

exit
