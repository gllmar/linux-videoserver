echo "exec--:>  `basename $0`"
#curl -XPOST -H "Content-type: application/json" -d '{"type":"Scene","name":"out_A","monitor":0}' 'http://127.0.0.1:4445/call/OpenProjector'

i3-msg '[class="^obs"] move [--no-auto-back-and-forth] to workspace 1'
sleep 1

curl -XPOST -H "Content-type: application/json" -d '{"type":"Scene","name":"out_A","monitor":-1}' 'http://127.0.0.1:4445/call/OpenProjector'
curl -XPOST -H "Content-type: application/json" -d '{"type":"Scene","name":"out_B","monitor":-1}' 'http://127.0.0.1:4445/call/OpenProjector'
curl -XPOST -H "Content-type: application/json" -d '{"type":"Scene","name":"out_C","monitor":-1}' 'http://127.0.0.1:4445/call/OpenProjector'

sleep 0.2
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_A$"] move to workspace 2'
sleep 0.2
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_B$"] move to workspace 3'
sleep 0.2 
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_C$"] move to workspace 4'

sleep 0.2
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_A$"] fullscreen toggle'
sleep 0.2
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_B$"] fullscreen toggle'
sleep 0.2 
i3-msg '[Title="^Windowed\\ Projector\\ \\(Scene\\)\\ \\-\\ out_C$"] fullscreen toggle'
